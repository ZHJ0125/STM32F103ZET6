#ifndef __LED_H			// 避免重复引用
#define __LED_H
#include "sys.h"

#define LED0 PBout(5)	// PB5
#define LED1 PEout(5)	// PE5

void LED_Init(void);	// 初始化


#endif
