#include "led.h"

// 初始化PB5和PE5为输出口.并使能这两个口的时钟		    
// LED IO初始化
void LED_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;					// 定义GPIO结构体成员

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB|RCC_APB2Periph_GPIOE, ENABLE);	 // 使能PB,PE端口时钟
//	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
//	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);
	
	// 配置GPIOB.5
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;				 // LED0-->PB.5 端口配置
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 // 推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 // IO口速度为50MHz
	GPIO_Init(GPIOB, &GPIO_InitStructure);					 // 根据设定参数初始化GPIOB.5
	GPIO_SetBits(GPIOB,GPIO_Pin_5);						 	 // PB.5 输出高 默认不点亮

	// 配置GPIOE.5
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;	    		 // LED1-->PE.5 端口配置, 推挽输出
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 // 推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 // IO口速度为50MHz
	GPIO_Init(GPIOE, &GPIO_InitStructure);	  				 // 推挽输出 ，IO口速度为50MHz
	GPIO_SetBits(GPIOE,GPIO_Pin_5); 						 // PE.5 输出高 默认不点亮
}




//*********************下面注释掉的代码是通过 直接操作寄存器 方式实现GPIO初始化***********************
//void LED_Init(void)
//{
//	// 使能时钟
//	RCC->APB2ENR |= 1<<3;		// 第三位置1
//	RCC->APB2ENR |= 1<<6;		// 第六位置1
//	
//	// 配置GPIO
//	GPIOB->CRL &= 0xFF0FFFFF;
//	GPIOB->CRL |= 0x00300000;	// 相当于GPIO_Init
//	GPIOB->ODR |= 1<<5;
//	
//	GPIOE->CRL &= 0xFF0FFFFF;
//	GPIOE->CRL |= 0x00300000;
//	GPIOE->ODR |= 1<<5;
//}
