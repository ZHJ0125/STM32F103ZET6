#include "beep.h"

// 初始化PB8为输出口.并使能这个IO口的时钟		    
// BEEP IO初始化
void BEEP_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;					// 定义GPIO结构体成员

//	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB|RCC_APB2Periph_GPIOE, ENABLE);	 // 使能PB,PE端口时钟
//	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);

	// 配置GPIOE.8
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;	    		 // BEEP-->PE.8 端口配置, 推挽输出
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 // 推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 // IO口速度为50MHz
	GPIO_Init(GPIOE, &GPIO_InitStructure);	  				 // 推挽输出 ，IO口速度为50MHz
	
	GPIO_ResetBits(GPIOE,GPIO_Pin_8); 						 // PE.8 输出0 默认不响
}




//*********************下面注释掉的代码是通过 直接操作寄存器 方式实现GPIO初始化***********************
//void LED_Init(void)
//{
//	// 使能时钟
//	RCC->APB2ENR |= 1<<3;		// 第三位置1
//	RCC->APB2ENR |= 1<<6;		// 第六位置1
//	
//	// 配置GPIO
//	GPIOB->CRL &= 0xFF0FFFFF;
//	GPIOB->CRL |= 0x00300000;	// 相当于GPIO_Init
//	GPIOB->ODR |= 1<<5;
//	
//	GPIOE->CRL &= 0xFF0FFFFF;
//	GPIOE->CRL |= 0x00300000;
//	GPIOE->ODR |= 1<<5;
//}
