#include "stm32f10x.h"
#include "beep.h"
#include "delay.h"

int main()
{
	delay_init();
	BEEP_Init();
	while(1)
	{
		GPIO_SetBits(GPIOE,GPIO_Pin_8);
		delay_ms(500);
		GPIO_ResetBits(GPIOE,GPIO_Pin_8);
		delay_ms(500);
	}
}
