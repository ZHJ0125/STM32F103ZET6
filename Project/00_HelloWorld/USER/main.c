#include "delay.h"
#include "usart.h"


// 主函数功能：串口每隔500ms递增发送数据
int main(void)
{
	u8 t=0;
	delay_init();
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	uart_init(115200);
	while(1)
	{
		printf("t:%d\n\n",t);
		delay_ms(500);
		t++;
	}
}

